[![pipeline status](https://gitlab.com/moreiraandre/tesis-api-contasp/badges/master/pipeline.svg)](https://gitlab.com/moreiraandre/tesis-api-contasp/-/pipelines/latest)
[![coverage report](https://gitlab.com/moreiraandre/tesis-api-contasp/badges/master/coverage.svg)](https://gitlab.com/moreiraandre/tesis-api-contasp/-/commits/master)

# API de Gestão de Contas a Pagar com PHP/Laravel
Este exemplo implementa serviços para gestão de **Fornecedores** e **Contas a Pagar**

## Requisitos
* [Requisitos Laravel](https://laravel.com/docs/6.x#server-requirements)
* [Composer](https://getcomposer.org/download/)

## Download & Instalação
Será solicitada a senha de `sudo`!
```sh
git clone git@gitlab.com:moreiraandre/tesis-api-contasp.git api-contasp ; cd api-contasp
chmod +x storage/bash/install
storage/bash/install
php artisan serve
```
> O último comando levanta o servidor do Laravel para a aplicação, basta utilizar o domínio gerado para fazer as
 requisições (em outro terminal) aos **endpoints**.

## Exemplos de requisição pelo terminal
```sh
curl --location --request GET 'http://127.0.0.1:8000/api/fornecedor?api_token=CODETESIS' --header 'X-Requested-With: XMLHttpRequest'
```

## Endpoints
### Fornecedores
| Method    | URI                                   |
| --------- | ------------------------------------- |
| GET       | api/fornecedor                        |
| POST      | api/fornecedor                        |
| GET       | api/fornecedor/{id}                   |
| PUT       | api/fornecedor/{id}                   |
| DELETE    | api/fornecedor/{id}                   |

Exemplo Payload
```json
 {
    "cnpj": "60729616000120",
    "nome": "Fornecedor #1",
    "fone": "1132230000",
    "email": "email@empresa.com",
    "endereco": "Rua Principal, 123 Centro"
}
```

### Contas a Pagar
| Method    | URI                                   |
| --------- | ------------------------------------- |
| POST      | api/contas-pagar                      |
| GET       | api/contas-pagar                      |
| PUT       | api/contas-pagar/pagar/{id}           |
| GET       | api/contas-pagar/{id}                 |
| PUT       | api/contas-pagar/{id}                 |
| DELETE    | api/contas-pagar/{id}                 |

Exemplo Payload
```json
 {
    "fornecedor_id": 6,
    "descricao": "Minha Conta para Pagar",
    "valor": 49.78,
    "data_vencimento": "2020-11-01"
}
```

## Diretórios das Implementações de Regras
* [app/Business](app/Business) - Serviços
* [app/Http/Controllers](app/Http/Controllers) - Roteamento das requisições
* [app/Http/Middleware/Prepare](app/Http/Middleware/Prepare) - Formatação dos campos da requisição
* [app/Http/Requests](app/Http/Requests) - Validação dos campos da requisição
* [app/Http/Resources](app/Http/Resources) - Formatação da resposta

## Testes
### Pastas
* [Unitários](tests/Unit)
* [Integração](tests/Feature)

### Execução
```
bin/phpunit --coverage-text --coverage-html storage/app/coverage
```
> Após executar o comando, vá no diretório `storage/app/coverage` e abra o `index.html` no navegador para visualizar o
 relatório de cobertura de testes.
