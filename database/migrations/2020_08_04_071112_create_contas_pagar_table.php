<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContasPagarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contas_pagar', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('fornecedor_id');
            $table->foreign('fornecedor_id')
                ->references('id')
                ->on('fornecedores');

            $table->string('descricao');
            $table->float('valor');
            $table->date('data_vencimento');
            $table->timestamp('paga_em')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
