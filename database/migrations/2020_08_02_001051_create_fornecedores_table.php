<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFornecedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cnpj', 25);
            $table->string('nome');
            $table->string('fone', 25)->nullable();
            $table->string('email', 35)->nullable();
            $table->string('endereco')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
