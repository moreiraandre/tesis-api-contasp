<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Fornecedor;
use Faker\Generator as Faker;

$factory->define(Fornecedor::class, function (Faker $faker) {
    return [
        'cnpj' => $faker->cnpj(false),
        'nome' => $faker->company,
        'fone' => $faker->phoneNumberCleared,
        'email' => $faker->companyEmail,
        'endereco' => $faker->address,
    ];
});
