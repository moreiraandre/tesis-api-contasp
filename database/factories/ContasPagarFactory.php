<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ContasPagar;
use App\Models\Fornecedor;
use Faker\Generator as Faker;

$factory->define(ContasPagar::class, function (Faker $faker) {
    $fornecedor = factory(Fornecedor::class)->create();

    return [
        'fornecedor_id' => $fornecedor->getKey(),
        'descricao' => $faker->paragraph(),
        'valor' => $faker->randomFloat(2, 1),
        'data_vencimento' => $faker->date(),
    ];
});
