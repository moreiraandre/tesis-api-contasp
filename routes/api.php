<?php

Route::apiResources([
    'fornecedor' => 'FornecedorController',
    'contas-pagar' => 'ContasPagarController',
]);

Route::put('contas-pagar/pagar/{contas_pagar}', 'ContasPagarController@pagar');
