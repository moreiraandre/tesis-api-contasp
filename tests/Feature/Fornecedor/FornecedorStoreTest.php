<?php

namespace Tests\Feature\Fornecedor;

use Illuminate\Foundation\Testing\RefreshDatabase;

class FornecedorStoreTest extends Fornecedor
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'POST';

    public function testStoreWithRequiredFields()
    {
        $data = [
            'cnpj' => '01793574000127',
            'nome' => 'Feature Test',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT, $data);
        $response->assertStatus(201);
        $this->assertDatabaseHas(self::DATABASE_TABLE, $data);
    }

    public function testStoreAllFields()
    {
        $data = [
            'cnpj' => '01793574000127',
            'nome' => 'Feature Test',
            'fone' => '6532230000',
            'email' => 'feature@test.com',
            'endereco' => 'Rua principal, 123 Centro',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT, $data);
        $response->assertStatus(201);
        $this->assertDatabaseHas(self::DATABASE_TABLE, $data);
    }

    public function testNotStoreRequiredFields()
    {
        $data = [
            'cnpj' => '',
            'nome' => '',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT, $data);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(422);
        foreach ($contentResponse['errors'] as $campo => $mensagemErro) {
            $this->assertTrue(strpos(current($mensagemErro), 'required') !== false);
            $this->assertTrue(in_array($campo, array_keys($data)));
        }
    }

    public function testNotStoreLargeFields()
    {
        $data = $this->getLargeData();

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT, $data);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(422);
        foreach ($contentResponse['errors'] as $campo => $mensagemErro) {
            $this->assertTrue(strpos(current($mensagemErro), 'may not be greater than') !== false);
            $this->assertTrue(in_array($campo, array_keys($data)));
        }
    }
}
