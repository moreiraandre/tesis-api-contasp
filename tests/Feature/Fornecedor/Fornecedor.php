<?php

namespace Tests\Feature\Fornecedor;

use App\Models\Fornecedor as Model;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

abstract class Fornecedor extends TestCase
{
    protected const BASE_ENDPOINT = '/api/fornecedor';
    protected const DATABASE_TABLE = 'fornecedores';

    /**
     * @var User
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $user = factory(User::class)->create();
        $this->user = $user;
    }

    /**
     * @return User
     */
    protected function getUser(): User
    {
        return $this->user;
    }

    protected function initStore()
    {
        DB::table(self::DATABASE_TABLE)->truncate();

        $data = factory(Model::class)->make()->toArray();

        $response = $this->actingAs($this->user)
            ->json('POST', self::BASE_ENDPOINT, $data);

        $contentResponse = $response->getOriginalContent();
        return $contentResponse->getKey();
    }

    /**
     * @return array
     */
    protected function getLargeData()
    {
        $longText = str_repeat('A', 300);
        $longNumber = str_repeat('1', 35);

        return [
            'cnpj' => $longNumber,
            'nome' => $longText,
            'fone' => $longNumber,
            'email' => "$longNumber@email.com",
            'endereco' => $longText,
        ];
    }
}
