<?php

namespace Tests\Feature\Fornecedor;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Fornecedor as Model;

class FornecedorUpdateTest extends Fornecedor
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'PUT';

    public function testUpdateName()
    {
        $id = $this->initStore();

        $data = [
            'nome' => 'Feature Test - Updated',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas(self::DATABASE_TABLE, $data);
    }

    public function testUpdateAllFields()
    {
        $id = $this->initStore();

        $data = factory(Model::class)->make()->toArray();

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas(self::DATABASE_TABLE, $data);
    }

    public function testNotUpdateNotFound()
    {
        $data = [
            'nome' => 'Feature Test - Updated',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/1', $data);
        $response->assertStatus(404);
    }

    public function testNotUpdateRequiredFields()
    {
        $id = $this->initStore();

        $data = [
            'cnpj' => '',
            'nome' => '',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id, $data);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(422);
        foreach ($contentResponse['errors'] as $campo => $mensagemErro) {
            $this->assertTrue(strpos(current($mensagemErro), 'field must have a value.') !== false);
            $this->assertTrue(in_array($campo, array_keys($data)));
        }
    }

    public function testNotUpdateLargeFields()
    {
        $id = $this->initStore();

        $data = $this->getLargeData();

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id, $data);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(422);
        foreach ($contentResponse['errors'] as $campo => $mensagemErro) {
            $this->assertTrue(strpos(current($mensagemErro), 'may not be greater than') !== false);
            $this->assertTrue(in_array($campo, array_keys($data)));
        }
    }
}
