<?php

namespace Tests\Feature\Fornecedor;

use Illuminate\Foundation\Testing\RefreshDatabase;

class FornecedorDeleteTest extends Fornecedor
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'DELETE';

    public function testDelete()
    {
        $id = $this->initStore();
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id);

        $response->assertStatus(200);
    }

    public function testNotDeleteNotFound()
    {
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/0');
        $response->assertStatus(404);
    }
}
