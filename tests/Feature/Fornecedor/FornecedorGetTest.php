<?php

namespace Tests\Feature\Fornecedor;

use Illuminate\Foundation\Testing\RefreshDatabase;

class FornecedorGetTest extends Fornecedor
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'GET';

    public function testList()
    {
        $this->initStore();
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(200);
        $this->assertTrue(!empty($contentResponse->toArray()));
    }

    public function testListEmpty()
    {
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(200);
        $this->assertTrue(empty($contentResponse->toArray()));
    }

    public function testShow()
    {
        $id = $this->initStore();
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id);
        $response->assertStatus(200);
    }

    public function testShowNotFound()
    {
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/1');
        $response->assertStatus(404);
    }
}
