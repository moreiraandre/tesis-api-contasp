<?php

namespace Tests\Feature\ContasPagar;

use Illuminate\Foundation\Testing\RefreshDatabase;

class ContasPagarDeleteTest extends ContasPagar
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'DELETE';

    public function testDelete()
    {
        $id = $this->initStore();
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id);

        $response->assertStatus(200);
    }

    public function testNotDeleteNotFound()
    {
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/1');
        $response->assertStatus(404);
    }
}
