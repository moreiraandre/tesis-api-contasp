<?php

namespace Tests\Feature\ContasPagar;

use App\Models\ContasPagar as Model;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContasPagarUpdateTest extends ContasPagar
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'PUT';

    public function testUpdateOneField()
    {
        $id = $this->initStore();

        $data = [
            'descricao' => 'Feature Test - Updated',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas(self::DATABASE_TABLE, $data);
    }

    public function testUpdateAllFields()
    {
        $id = $this->initStore();

        $data = factory(Model::class)->make()->toArray();

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas(self::DATABASE_TABLE, $data);
    }

    public function testNotUpdateNotFound()
    {
        $data = [];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/0', $data);
        $response->assertStatus(404);
    }

    public function testNotUpdateRequiredFields()
    {
        $id = $this->initStore();

        $data = [
            'descricao' => '',
            'valor' => '',
            'data_vencimento' => '',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id, $data);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(422);
        foreach ($contentResponse['errors'] as $campo => $mensagemErro) {
            $this->assertTrue(strpos(current($mensagemErro), 'field must have a value.') !== false);
            $this->assertTrue(in_array($campo, array_keys($data)));
        }
    }

    public function testNotUpdateLargeFields()
    {
        $id = $this->initStore();

        $data = $this->getLargeData();

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id, $data);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(422);
        foreach ($contentResponse['errors'] as $campo => $mensagemErro) {
            $this->assertTrue(strpos(current($mensagemErro), 'may not be greater than') !== false);
            $this->assertTrue(in_array($campo, array_keys($data)));
        }
    }
}
