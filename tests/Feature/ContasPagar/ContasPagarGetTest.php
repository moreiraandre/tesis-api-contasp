<?php

namespace Tests\Feature\ContasPagar;

use Illuminate\Foundation\Testing\RefreshDatabase;

class ContasPagarGetTest extends ContasPagar
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'GET';

    public function testList()
    {
        $this->initStore();
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(200);
        $this->assertTrue(!empty($contentResponse->toArray()));
    }

    public function testListWithFilter()
    {
        $this->initStore();
        $filtro = '?filtros=paga:true';
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . $filtro);

        $response->assertStatus(200);
    }

    public function testListWithOrder()
    {
        $this->initStore();
        $ordem = '?ordenacao=paga:asc';
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . $ordem);

        $response->assertStatus(200);
    }

    public function testListEmpty()
    {
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(200);
        $this->assertTrue(empty($contentResponse->toArray()));
    }

    public function testShow()
    {
        $id = $this->initStore();
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/' . $id);
        $response->assertStatus(200);
    }

    public function testShowNotFound()
    {
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . '/1');
        $response->assertStatus(404);
    }
}
