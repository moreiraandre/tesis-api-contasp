<?php

namespace Tests\Feature\ContasPagar;

use App\Models\ContasPagar as Model;
use App\Models\Fornecedor as FornecedorModel;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContasPagarStoreTest extends ContasPagar
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'POST';

    public function testStoreWithRequiredFields()
    {
        $fornecedor = factory(FornecedorModel::class)->create();
        $data = [
            'fornecedor_id' => $fornecedor->getKey(),
            'descricao' => 'Minha conta',
            'valor' => 12.20,
            'data_vencimento' => date('Y-m-d'),
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT, $data);
        $response->assertStatus(201);
        $this->assertDatabaseHas(self::DATABASE_TABLE, $data);
    }

    public function testStoreAllFields()
    {
        $data = factory(Model::class)->make()->toArray();

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT, $data);
        $response->assertStatus(201);
        $this->assertDatabaseHas(self::DATABASE_TABLE, $data);
    }

    public function testNotStoreRequiredFields()
    {
        $data = [
            'fornecedor_id' => '',
            'descricao' => '',
            'valor' => '',
            'data_vencimento' => '',
        ];

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT, $data);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(422);
        foreach ($contentResponse['errors'] as $campo => $mensagemErro) {
            $this->assertTrue(strpos(current($mensagemErro), 'required') !== false);
            $this->assertTrue(in_array($campo, array_keys($data)));
        }
    }

    public function testNotStoreLargeFields()
    {
        $data = $this->getLargeData();

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT, $data);
        $contentResponse = $response->getOriginalContent();

        $response->assertStatus(422);
        foreach ($contentResponse['errors'] as $campo => $mensagemErro) {
            $this->assertTrue(strpos(current($mensagemErro), 'may not be greater than') !== false);
            $this->assertTrue(in_array($campo, array_keys($data)));
        }
    }
}
