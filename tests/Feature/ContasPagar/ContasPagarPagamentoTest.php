<?php

namespace Tests\Feature\ContasPagar;

use Illuminate\Foundation\Testing\RefreshDatabase;

class ContasPagarPagamentoTest extends ContasPagar
{
    use RefreshDatabase;

    private const VERBO_HTTP = 'PUT';
    private const ENDPOINT = '/pagar';

    public function testPayment()
    {
        $id = $this->initStore();

        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . self::ENDPOINT . '/' . $id);
        $response->assertStatus(200);
    }

    public function testNotPaymentNotFound()
    {
        $response = $this->actingAs($this->getUser())
            ->json(self::VERBO_HTTP, self::BASE_ENDPOINT . self::ENDPOINT . '/0');
        $response->assertStatus(404);
    }
}
