<?php

namespace Tests\Feature\ContasPagar;

use App\Models\ContasPagar as Model;
use App\Models\User;
use Faker\Factory;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

abstract class ContasPagar extends TestCase
{
    protected const BASE_ENDPOINT = '/api/contas-pagar';
    protected const DATABASE_TABLE = 'contas_pagar';

    /**
     * @var User
     */
    private $user;

    /**
     * @return User
     */
    protected function getUser(): User
    {
        return $this->user;
    }

    protected function setUp(): void
    {
        parent::setUp();

        $user = factory(User::class)->create();
        $this->user = $user;
    }

    protected function initStore()
    {
        DB::table(self::DATABASE_TABLE)->truncate();

        $data = factory(Model::class)->make()->toArray();

        $response = $this->actingAs($this->user)
            ->json('POST', self::BASE_ENDPOINT, $data);

        $contentResponse = $response->getOriginalContent();
        return $contentResponse->getKey();
    }

    /**
     * @return array
     */
    protected function getLargeData()
    {
        $faker = Factory::create('pt_BR');
        return factory(Model::class)->make(['descricao' => str_repeat('A', 300)])->toArray();
    }
}
