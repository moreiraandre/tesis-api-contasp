<?php

namespace Tests\Unit\ContasPagar;

use App\Business\ContasPagar\Listagem;
use App\Models\ContasPagar as Model;
use Illuminate\Http\Request;
use ReflectionClass;
use Tests\TestCase;

class ListagemTest extends TestCase
{
    private function getReflection($objeto)
    {
        return new ReflectionClass($objeto);
    }

    public function testOrdenarDataVencimento()
    {
        $dataVencimento = '2020-12-01';

        $this->mock(Request::class, function ($mock) use ($dataVencimento) {
            $mock->shouldReceive('has')->with('ordenacao.data_vencimento')->once()->andReturn(true);
            $mock->shouldReceive('has')->with('ordenacao.pago_em')->once();
            $mock->shouldReceive('input')->with('ordenacao.data_vencimento')->once()->andReturn($dataVencimento);
        });

        $this->mock(Model::class, function ($mock) use ($dataVencimento) {
            $mock->shouldReceive('orderBy')->with('data_vencimento', $dataVencimento)->once();
        });

        $service = app(Listagem::class);
        $reflection = $this->getReflection($service);

        $method = $reflection->getMethod('ordenar');
        $method->setAccessible(true);
        $method->invoke($service);
    }

    public function testOrdenarPagoEm()
    {
        $tipoOrdenacao = 'asc';

        $this->mock(Request::class, function ($mock) use ($tipoOrdenacao) {
            $mock->shouldReceive('has')->with('ordenacao.data_vencimento')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('ordenacao.pago_em')->once()->andReturn(true);
            $mock->shouldReceive('input')->with('ordenacao.pago_em')->once()->andReturn($tipoOrdenacao);
        });

        $this->mock(Model::class, function ($mock) use ($tipoOrdenacao) {
            $mock->shouldReceive('orderBy')->with('pago_em', $tipoOrdenacao)->once();
        });

        $service = app(Listagem::class);
        $reflection = $this->getReflection($service);

        $method = $reflection->getMethod('ordenar');
        $method->setAccessible(true);
        $method->invoke($service);
    }

    public function testFiltrarFornecedor()
    {
        $fornecedorId = 1;

        $this->mock(Request::class, function ($mock) use ($fornecedorId) {
            $mock->shouldReceive('has')->with('filtros.fornecedor_id')->once()->andReturn(true);
            $mock->shouldReceive('has')->with('filtros.paga')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_inicial')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_final')->once()->andReturn(false);
            $mock->shouldReceive('input')->with('filtros.fornecedor_id')->once()->andReturn($fornecedorId);
        });

        $this->mock(Model::class, function ($mock) use ($fornecedorId) {
            $mock->shouldReceive('where')->with('fornecedor_id', $fornecedorId)->once();
        });

        $service = app(Listagem::class);
        $reflection = $this->getReflection($service);

        $method = $reflection->getMethod('filtrar');
        $method->setAccessible(true);
        $method->invoke($service);
    }

    public function testFiltrarContasPagas()
    {
        $this->mock(Request::class, function ($mock) {
            $mock->shouldReceive('has')->with('filtros.fornecedor_id')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.paga')->once()->andReturn(true);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_inicial')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_final')->once()->andReturn(false);
            $mock->shouldReceive('input')->with('filtros.paga')->once()->andReturn(true);
        });

        $this->mock(Model::class, function ($mock) {
            $mock->shouldReceive('whereNotNull')->with('paga_em')->once();
        });

        $service = app(Listagem::class);
        $reflection = $this->getReflection($service);

        $method = $reflection->getMethod('filtrar');
        $method->setAccessible(true);
        $method->invoke($service);
    }

    public function testFiltrarContasNaoPagas()
    {
        $this->mock(Request::class, function ($mock) {
            $mock->shouldReceive('has')->with('filtros.fornecedor_id')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.paga')->once()->andReturn(true);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_inicial')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_final')->once()->andReturn(false);
            $mock->shouldReceive('input')->with('filtros.paga')->once()->andReturn(false);
        });

        $this->mock(Model::class, function ($mock) {
            $mock->shouldReceive('whereNull')->with('paga_em')->once();
        });

        $service = app(Listagem::class);
        $reflection = $this->getReflection($service);

        $method = $reflection->getMethod('filtrar');
        $method->setAccessible(true);
        $method->invoke($service);
    }

    public function testFiltrarDataVencimentoInicial()
    {
        $dataVencimentoInicial = '2020-12-01';

        $this->mock(Request::class, function ($mock) use ($dataVencimentoInicial) {
            $mock->shouldReceive('has')->with('filtros.fornecedor_id')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.paga')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_inicial')->once()->andReturn(true);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_final')->once()->andReturn(false);
            $mock->shouldReceive('input')->with('filtros.data_vencimento_inicial')->once()->andReturn($dataVencimentoInicial);
        });

        $this->mock(Model::class, function ($mock) use ($dataVencimentoInicial) {
            $mock->shouldReceive('where')->with('data_vencimento', '>=', $dataVencimentoInicial)->once();
        });

        $service = app(Listagem::class);
        $reflection = $this->getReflection($service);

        $method = $reflection->getMethod('filtrar');
        $method->setAccessible(true);
        $method->invoke($service);
    }

    public function testFiltrarDataVencimentoFinal()
    {
        $dataVencimentoFinal = '2020-12-30';

        $this->mock(Request::class, function ($mock) use ($dataVencimentoFinal) {
            $mock->shouldReceive('has')->with('filtros.fornecedor_id')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.paga')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_inicial')->once()->andReturn(false);
            $mock->shouldReceive('has')->with('filtros.data_vencimento_final')->once()->andReturn(true);
            $mock->shouldReceive('input')->with('filtros.data_vencimento_final')->once()->andReturn($dataVencimentoFinal);
        });

        $this->mock(Model::class, function ($mock) use ($dataVencimentoFinal) {
            $mock->shouldReceive('where')->with('data_vencimento', '<=', $dataVencimentoFinal)->once();
        });

        $service = app(Listagem::class);
        $reflection = $this->getReflection($service);

        $method = $reflection->getMethod('filtrar');
        $method->setAccessible(true);
        $method->invoke($service);
    }

    public function testListar()
    {
        $this->mock(Model::class, function ($mock) {
            $mock->shouldReceive('paginate')->with()->once();
        });

        $service = app(Listagem::class);
        $service->listar();
    }
}
