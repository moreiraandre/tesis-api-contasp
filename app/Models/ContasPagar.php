<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContasPagar extends Model
{
    use SoftDeletes;

    protected $table = 'contas_pagar';

    protected $fillable = [
        'fornecedor_id',
        'descricao',
        'valor',
        'data_vencimento',
    ];

    protected $dates = ['paga_em'];

    protected $casts = [
        'fornecedor_id' => 'integer',
        'valor' => 'real',
    ];

    public function fornecedor()
    {
        return $this->hasOne(Fornecedor::class, 'id', 'fornecedor_id');
    }
}
