<?php

namespace App\Business\ContasPagar;

use App\Models\ContasPagar as Model;
use Illuminate\Http\Request;

class Listagem
{
    /**
     * @var Model
     */
    private $model;

    /**
     * @var Request
     */
    private $request;

    public function __construct(Model $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }

    /**
     * @return Model
     */
    public function listar()
    {
        $this->filtrar();
        $this->ordenar();
        return $this->model->paginate();
    }

    private function filtrar()
    {
        if ($this->request->has('filtros.fornecedor_id')) {
            $this->model = $this->model->where('fornecedor_id', $this->request->input('filtros.fornecedor_id'));
        }

        if ($this->request->has('filtros.paga')) {
            if ($this->request->input('filtros.paga')) {
                $this->model = $this->model->whereNotNull('paga_em');
            } else {
                $this->model = $this->model->whereNull('paga_em');
            }
        }

        if ($this->request->has('filtros.data_vencimento_inicial')) {
            $this->model = $this->model->where('data_vencimento', '>=', $this->request->input('filtros.data_vencimento_inicial'));
        }

        if ($this->request->has('filtros.data_vencimento_final')) {
            $this->model = $this->model->where('data_vencimento', '<=', $this->request->input('filtros.data_vencimento_final'));
        }
    }

    private function ordenar()
    {
        if ($this->request->has('ordenacao.data_vencimento')) {
            $this->model = $this->model->orderBy('data_vencimento', $this->request->input('ordenacao.data_vencimento'));
        }

        if ($this->request->has('ordenacao.pago_em')) {
            $this->model = $this->model->orderBy('pago_em', $this->request->input('ordenacao.pago_em'));
        }
    }
}
