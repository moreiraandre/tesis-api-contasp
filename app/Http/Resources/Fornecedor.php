<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Fornecedor extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cnpj' => $this->cnpj,
            'nome' => $this->nome,
            'fone' => $this->fone,
            'email' => $this->email,
            'endereco' => $this->endereco,
            'created_at' => $this->created_at->format('Y-m-d H:i'),
        ];
    }
}
