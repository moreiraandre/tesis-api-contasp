<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PagarConta extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fornecedor_id' => $this->fornecedor_id,
            'descricao' => $this->descricao,
            'valor' => $this->valor,
            'data_vencimento' => $this->data_vencimento,
            'paga_em' => $this->paga_em->format('Y-m-d'),
            'created_at' => $this->created_at->format('Y-m-d H:i'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i'),
        ];
    }
}
