<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FornecedorStore extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cnpj' => [
                'required',
                'max:25',
                Rule::unique('fornecedores', 'cnpj')->where(function ($query) {
                    $query->whereNull('deleted_at');
                })
            ],
            'nome' => 'required|max:255',
            'fone' => 'nullable|max:25',
            'email' => 'nullable|email|max:35',
            'endereco' => 'nullable|max:255',
        ];
    }
}
