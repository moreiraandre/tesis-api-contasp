<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContasPagarUpdate extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fornecedor_id' => [
                'filled',
                Rule::exists('fornecedores', 'id')->where(function ($query) {
                    $query->whereNull('deleted_at');
                })
            ],
            'descricao' => 'filled|max:255',
            'valor' => 'filled|numeric|gt:0',
            'data_vencimento' => 'filled|date',
        ];
    }
}
