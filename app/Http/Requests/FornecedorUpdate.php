<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FornecedorUpdate extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cnpj' => [
                'filled',
                'max:25',
                Rule::unique('fornecedores', 'cnpj')->where(function ($query) {
                    $query->whereNull('deleted_at');
                    $query->where('id', '!=', $this->fornecedor->getKey());
                })
            ],
            'nome' => 'filled|max:255',
            'fone' => 'filled|max:25',
            'email' => 'filled|email|max:35',
            'endereco' => 'filled|max:255',
        ];
    }
}
