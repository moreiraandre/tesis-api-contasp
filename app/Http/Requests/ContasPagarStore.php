<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContasPagarStore extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fornecedor_id' => [
                'required',
                Rule::exists('fornecedores', 'id')->where(function ($query) {
                    $query->whereNull('deleted_at');
                })
            ],
            'descricao' => 'required|max:255',
            'valor' => 'required|numeric|gt:0',
            'data_vencimento' => 'required|date',
        ];
    }
}
