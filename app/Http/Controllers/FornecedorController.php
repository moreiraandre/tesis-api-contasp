<?php

namespace App\Http\Controllers;

use App\Http\Middleware\Prepare\Fornecedor as Prepare;
use App\Http\Requests\FornecedorStore;
use App\Http\Requests\FornecedorUpdate;
use App\Http\Resources\Fornecedor as FornecedorResource;
use App\Models\Fornecedor;

class FornecedorController extends Controller
{
    public function __construct()
    {
        $this->middleware(Prepare::class);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return FornecedorResource::collection(Fornecedor::paginate());
    }

    /**
     * @param FornecedorStore $request
     * @return FornecedorResource
     */
    public function store(FornecedorStore $request)
    {
        $fornecedor = Fornecedor::create($request->all());
        return new FornecedorResource($fornecedor);
    }

    /**
     * @param Fornecedor $fornecedor
     * @return FornecedorResource
     */
    public function show(Fornecedor $fornecedor)
    {
        return new FornecedorResource($fornecedor);
    }

    /**
     * @param FornecedorUpdate $request
     * @param Fornecedor $fornecedor
     * @return FornecedorResource
     */
    public function update(FornecedorUpdate $request, Fornecedor $fornecedor)
    {
        $fornecedor->update($request->all());
        return new FornecedorResource($fornecedor);
    }

    /**
     * @param Fornecedor $fornecedor
     * @return FornecedorResource
     * @throws \Exception
     */
    public function destroy(Fornecedor $fornecedor)
    {
        $fornecedor->delete();
        return new FornecedorResource($fornecedor);
    }
}
