<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContasPagarStore;
use App\Http\Requests\ContasPagarUpdate;
use App\Http\Resources\ContasPagar as ContasPagarResource;
use App\Http\Resources\PagarConta as PagarContaResource;
use App\Models\ContasPagar;
use App\Business\ContasPagar\Listagem;

class ContasPagarController extends Controller
{
    /**
     * @param Listagem $listagem
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Listagem $listagem)
    {
        return ContasPagarResource::collection($listagem->listar());
    }

    /**
     * @param ContasPagarStore $request
     * @return ContasPagarResource
     */
    public function store(ContasPagarStore $request)
    {
        $contasPagar = ContasPagar::create($request->all());
        return new ContasPagarResource($contasPagar);
    }

    /**
     * @param ContasPagar $contasPagar
     * @return ContasPagarResource
     */
    public function show(ContasPagar $contasPagar)
    {
        return new ContasPagarResource($contasPagar);
    }

    /**
     * @param ContasPagarUpdate $request
     * @param ContasPagar $contasPagar
     * @return ContasPagarResource
     */
    public function update(ContasPagarUpdate $request, ContasPagar $contasPagar)
    {
        $contasPagar->update($request->all());
        return new ContasPagarResource($contasPagar);
    }

    /**
     * @param ContasPagar $contasPagar
     * @return ContasPagarResource
     * @throws \Exception
     */
    public function destroy(ContasPagar $contasPagar)
    {
        $contasPagar->delete();
        return new ContasPagarResource($contasPagar);
    }

    /**
     * @param ContasPagar $contasPagar
     * @return PagarContaResource
     */
    public function pagar(ContasPagar $contasPagar)
    {
        $contasPagar->paga_em = now();
        $contasPagar->save();
        return new PagarContaResource($contasPagar);
    }
}
