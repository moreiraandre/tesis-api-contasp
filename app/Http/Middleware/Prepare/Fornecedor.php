<?php

namespace App\Http\Middleware\Prepare;

use Closure;
use Illuminate\Http\Request;

class Fornecedor
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->cnpj($request);
        $this->fone($request);

        return $next($request);
    }

    private function cnpj(Request $request)
    {
        if ($request->has('cnpj')) {
            $request->merge(['cnpj' => preg_replace('/\D/', '', $request->input('cnpj'))]);
        }
    }

    private function fone(Request $request)
    {
        if ($request->has('fone')) {
            $request->merge(['fone' => preg_replace('/\D/', '', $request->input('fone'))]);
        }
    }
}
