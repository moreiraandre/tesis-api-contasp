<?php

namespace App\Http\Middleware\Prepare;

use Closure;
use Illuminate\Http\Request;

class Ordenacao
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('ordenacao')) {
            $ordenacaoRequest = explode(',', $request->input('ordenacao'));
            foreach ($ordenacaoRequest as $indice => $criterio) {
                list($chaveOrdenacao, $valorOrdenacao) = explode(':', $criterio);
                $request->merge([
                    'ordenacao' => [$chaveOrdenacao => $valorOrdenacao]
                ]);
            }
        }

        return $next($request);
    }
}
