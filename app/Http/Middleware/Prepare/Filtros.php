<?php

namespace App\Http\Middleware\Prepare;

use Closure;
use Illuminate\Http\Request;

class Filtros
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('filtros')) {
            $filtrosRequest = explode(',', $request->input('filtros'));
            foreach ($filtrosRequest as $indice => $filtro) {
                list($chaveFiltro, $valorFiltro) = explode(':', $filtro);
                $request->merge([
                    'filtros' => [$chaveFiltro => $valorFiltro]
                ]);
            }
        }

        return $next($request);
    }
}
